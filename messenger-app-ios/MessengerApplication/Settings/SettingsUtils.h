
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define SETTINGS_STORYBOARD @"settings"

@interface SettingsUtils : NSObject

+(UIStoryboard *) getMeSettingsStoryBoard ;
+(NSBundle *)getBundle ;
+ (UIImage *)imageNamed:(NSString *)imageName;


@end
