

#import <Foundation/Foundation.h>
#import <PushKit/PushKit.h>

@interface SamplePushKitNotify : NSObject <PKPushRegistryDelegate>
+(SamplePushKitNotify *)getInstance;
@end
