
#import <UIKit/UIKit.h>

typedef void (^LaunchMesiboBlock)();

@interface EditSelfProfileViewController : UIViewController <UIGestureRecognizerDelegate>


- (void) setStatusLabel:(NSString *)mStatusText;
- (void) setSelfUserName:(NSString *)mSelfUserNameText;

@property (strong, nonatomic) LaunchMesiboBlock mLaunchMesibo ;

- (void) setLaunchMesiboCallback:(LaunchMesiboBlock) handler;
@end
