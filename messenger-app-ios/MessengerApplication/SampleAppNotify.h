
#import <Foundation/Foundation.h>
#import "Mesibo/Mesibo.h"
#import <UserNotifications/UserNotifications.h>

// both defined 0, sync up with Android
#define SAMPLEAPP_NOTIFYTYPE_MESSAGE    0
#define SAMPLEAPP_NOTIFYTYPE_OTHER    0

#define SampleAppNotifyInstance [SampleAppNotify getInstance]


@interface SampleAppNotify : NSObject <UNUserNotificationCenterDelegate>

+(SampleAppNotify *) getInstance;

-(void) notify:(int)type subject:(NSString *)subject message:(NSString *)message;
-(void) notifyMessage:(MesiboParams *)params message:(NSString *)message;
-(void) clear;

@end
