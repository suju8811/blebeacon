

#import <UIKit/UIKit.h>

@interface AppAlert :NSObject


+ (void)showDialogue:(NSString*)message withTitle :(NSString *) title;
+ (void)showDialogue:(NSString*)message withTitle:(NSString *)title handler:(void (^) (void)) handler;
@end
