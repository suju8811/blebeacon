
package org.mesibo.messenger.AppSettings;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.preference.PreferenceScreen;
import android.support.v7.preference.SwitchPreferenceCompat;

import org.mesibo.messenger.SampleAPI;
import org.mesibo.messenger.R;

public class DataUsageFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

        SharedPreferences sharedPreferences;


        @Override
        public void onCreatePreferences(Bundle bundle, String s) {
                //add xml
                final ActionBar ab = ((AppCompatActivity) (getActivity())).getSupportActionBar();
                ab.setDisplayHomeAsUpEnabled(true);
                ab.setTitle("Data usage settings");

                addPreferencesFromResource(R.xml.data_usage);
                sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

                Preference preference = findPreference("auto");
                PreferenceScreen preferenceScreen = (PreferenceScreen) findPreference("preferenceScreen");
                PreferenceCategory myPrefCatcell = (PreferenceCategory) findPreference("preferenceCategorycell");
                PreferenceCategory myPrefCatwifi = (PreferenceCategory) findPreference("preferenceCategorywifi");
                PreferenceCategory myPrefCatroam = (PreferenceCategory) findPreference("preferenceCategoryroam");

                //temporary
                preferenceScreen.removePreference(myPrefCatcell);
                preferenceScreen.removePreference(myPrefCatwifi);
                preferenceScreen.removePreference(myPrefCatroam);
        }

        @Override
        public void onResume() {
                super.onResume();
                //unregister the preferenceChange listener
                getPreferenceScreen().getSharedPreferences()
                        .registerOnSharedPreferenceChangeListener(this);
                displaySwitches();
        }


        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

                PreferenceScreen preferenceScreen = (PreferenceScreen) findPreference("preferenceScreen");
                PreferenceCategory myPrefCatcell = (PreferenceCategory) findPreference("preferenceCategorycell");
                PreferenceCategory myPrefCatwifi = (PreferenceCategory) findPreference("preferenceCategorywifi");
                PreferenceCategory myPrefCatroam = (PreferenceCategory) findPreference("preferenceCategoryroam");

                Preference preference = findPreference(key);
                if (preference instanceof SwitchPreferenceCompat && key.equalsIgnoreCase("auto")) {
                        SwitchPreferenceCompat datausage = (SwitchPreferenceCompat) preference;

                        boolean enabled = datausage.isChecked();
                        SampleAPI.setMediaAutoDownload(enabled);

                        try {
                                //when this is null
                                myPrefCatcell.setEnabled(!enabled);
                                myPrefCatwifi.setEnabled(!enabled);
                                myPrefCatroam.setEnabled(!enabled);
                        } catch (Exception e) {}


                }

        }

        @Override
        public void onPause() {
                super.onPause();
                //unregister the preference change listener
                getPreferenceScreen().getSharedPreferences()
                        .unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onDestroy() {
                super.onDestroy();
                //unregister event bus.
        }


        public void displaySwitches() {
                Preference preference = findPreference("auto");

                boolean autoDownload = SampleAPI.getMediaAutoDownload();
                //preference.setEnabled(autoDownload);
                SwitchPreferenceCompat datausage = (SwitchPreferenceCompat) preference;
                datausage.setChecked(autoDownload);
        }

}