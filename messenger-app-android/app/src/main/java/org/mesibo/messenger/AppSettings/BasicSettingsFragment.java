package org.mesibo.messenger.AppSettings;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mesibo.api.Mesibo;
import com.mesibo.api.MesiboUtils;
import com.mesibo.emojiview.EmojiconTextView;
import org.mesibo.messenger.EditProfileFragment;
import org.mesibo.messenger.SampleAPI;
import org.mesibo.messenger.R;


public class BasicSettingsFragment extends Fragment {


    private EmojiconTextView mUserName;
    private EmojiconTextView mUserStatus;
    private ImageView mUserImage;
    private Mesibo.UserProfile mUser = Mesibo.getSelfProfile();

    public BasicSettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_basic_settings, container, false);
        final ActionBar ab = ((AppCompatActivity)(getActivity())).getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle("Settings");

        if(null == mUser) {
            getActivity().finish();
            //TBD show alert
            return v;
        }

        mUserName = (EmojiconTextView) v.findViewById(R.id.set_self_user_name);
        mUserStatus = (EmojiconTextView) v.findViewById(R.id.set_self_status);
        mUserImage = (ImageView) v.findViewById(R.id.set_user_image);



        LinearLayout profileLayout = (LinearLayout) v.findViewById(R.id.set_picture_name_status_layout);
        profileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditProfileFragment RegFragment = new EditProfileFragment();
                ((SettingsActivity)getActivity()).setRequestingFragment(RegFragment);
                RegFragment.activateInSettingsMode();
                FragmentManager fm =((AppCompatActivity)(getActivity())).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.settings_fragment_place,RegFragment,"null");
                ft.addToBackStack("profile");
                ft.commit();

            }
        });

        LinearLayout DataUsageLayout = (LinearLayout) v.findViewById(R.id.set_data_layout);
        DataUsageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DataUsageFragment dataFragment = new DataUsageFragment();
                FragmentManager fm =((AppCompatActivity)(getActivity())).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.settings_fragment_place,dataFragment,"null");
                ft.addToBackStack("datausage");
                ft.commit();

            }
        });

        LinearLayout aboutLayout = (LinearLayout) v.findViewById(R.id.set_about_layout);
        aboutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AboutFragment aboutFragment = new AboutFragment();
                FragmentManager fm =((AppCompatActivity)(getActivity())).getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.settings_fragment_place,aboutFragment,"null");
                ft.addToBackStack("about");
                ft.commit();

            }
        });

        LinearLayout logoutLayout = (LinearLayout) v.findViewById(R.id.set_logout_layout);
        logoutLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SampleAPI.startLogout();
                getActivity().finish();
            }
        });


        return  v;
    }

    /**
     * Called when the fragment is visible to the user and actively running.
     * This is generally
     * tied to {@link Activity#onResume() Activity.onResume} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onResume() {
        super.onResume();
        mUser = Mesibo.getSelfProfile();
        String imagePath = Mesibo.getUserProfilePicturePath(mUser, Mesibo.FileInfo.TYPE_AUTO);
        if(null != imagePath) {
            Bitmap b = BitmapFactory.decodeFile(imagePath);
            if(null != b)
                mUserImage.setImageDrawable(MesiboUtils.getRoundImageDrawable(b));
        }

        if(!TextUtils.isEmpty(mUser.name)) {
            mUserName.setText(mUser.name);
        }else {
            mUserName.setText("");
        }

        if(!TextUtils.isEmpty(mUser.status)) {
            mUserStatus.setText(mUser.status);
        }else {
            mUserStatus.setText("");
        }
    }
}
